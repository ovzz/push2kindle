'use strict'

const WebSocket = require('ws')
const uuid = require('uuid/v4')

class WSServer {
  constructor (server) {
    const wss = new WebSocket.Server({ server })
    this.wss = wss
    this.clients = []

    //  WebSocket first connect handle
    wss.on('connection', (ws) => {
      //  Push new ws client to clients
      ws.custmerData = {}
      this.addClient(ws)

      const id = uuid()
      ws.id = id

      ws.send(JSON.stringify({ id }))

      ws.on('message', (msg) => {
        const data = JSON.parse(msg)
        const { id: reid } = data
        if (reid) {
          //  This socket is an reconnected client
          const oldws = this.findClientById(reid)
          const { custmerData } = oldws

          //  Remove from clients
          this.removeClientById(reid)

          //  Replace id
          ws.id = reid
          ws.custmerData = custmerData

          //  Send custmer data
          ws.send(JSON.stringify({ data: custmerData }))
        }
      })

      ws.on('close', (code, reason) => {
        //  Check close code isn't normal. src: "https://developer.mozilla.org/zh-CN/docs/Web/API/CloseEvent"
        if (code <= 1000 || code === 1005) {
          //  Client ws exit normally, remove from this.clients
          this.removeClientById(ws.id)
          return
        }
        console.info(`${ws.id} closed! reason: ${reason}, code: ${code}`)
      })
    })
  }

  sendMsgById (id, msg) {
    const ws = this.findClientById(id)
    if (ws === undefined) {
      throw new Error(`Can't find match client: ${id}`)
    }
    ws.send(JSON.stringify(msg))
  }

  sendMsgByIdQuiet (id, msg) {
    let v
    try {
      this.sendMsgById(id, msg)
      v = true
    } catch (e) {
      console.log(e)
      v = false
    }
    return v
  }

  addClient (ws) {
    this.clients.push(ws)
  }

  removeClientById (id) {
    //  Maybe has problem
    this.clients.forEach((i, idx, arr) => {
      if (id === i.id) {
        arr.splice(idx, 1)
      }
    })
  }

  findClientById (id) {
    return this.clients.find(v => v.id === id)
  }

  getClientDataById (id) {
    return this.findClientById(id).custmerData
  }

  setClientDataById (id, value) {
    this.findClientById(id).custmerData = value
  }
}

module.exports = WSServer
