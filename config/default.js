'use strict'

const path = require('path')

module.exports = {
  file: {
    path: path.join(__dirname, '..', 'tmp'),
    whiteList: ['zip', 'txt', 'mobi', 'azw', 'doc', 'docx', 'html', 'htm', 'rtf', 'jpeg', 'jpg', 'gif', 'png', 'bmp', 'pdf'],
    limit: '50mb'
  },
  email: {
    whiteList: ['kindle.cn', 'kindle.com']
  },
  server: {
    addr: '0.0.0.0',
    port: '3000'
  }
}
