'use strict'

const http = require('http')
const https = require('https')
const fs = require('fs')
const { URL } = require('url')

const fileType = require('file-type')
const uuid = require('uuid/v4')
const bytes = require('bytes')
const progress = require('progress-stream')

const Mailer = require('./mail')

const defaultHeaders = {
  Accept: 'Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
  'Accept-Encoding': 'gzip, deflate',
  'Accept-Language': 'zh-CN,zh;q=0.9,en;q=0.8,zh-TW;q=0.7',
  Connection: 'keep-alive',
  Cookie: 'SnVzdCBGb3IgU3R1ZHksIFRoYW5rcyE=',
  'Upgrade-Insecure-Requests': '1',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36'
}

function main ({ app, fileURL: url, headers: reqHeaders, fileName = `${uuid()}.noname`, toMail, fromMail, fromPwd, fromSmtp, wsid }) {
  const { ws } = app
  const fileConfig = app.config.file
  const request = url.startsWith('https://') ? https : http
  let dest
  let name

  const options = new URL(url)
  options.headers = Object.assign({}, defaultHeaders, reqHeaders)

  request.get(options, (res) => {
    const { headers } = res

    //  Get file length
    const { limit = '50mb', whiteList } = fileConfig
    const length = headers['content-length'] || 0

    if (bytes(length) > bytes(limit)) {
      ws.sendMsgByIdQuiet(wsid, { error: `File length can't more than ${limit}` })
      return
    }

    //  Get filename
    const disposition = headers['content-disposition'] || {}
    const filenameRaw = disposition.split(';').find(item => item.startsWith('filename')) || ''
    const filenameDecode = decodeURI(filenameRaw)
    const filename = filenameDecode.slice(filenameDecode.indexOf('=') + 1)
    name = filename !== '' ? filename : fileName
    const fixname = Date.now().toString() + name //  Add prefix
    const uploadDir = fileConfig.path

    //  Config progress
    let str = false
    if (length) {
      str = progress({
        length,
        time: 100
      })

      str.on('progress', p => {
        //  WebSocket send data
        ws.sendMsgByIdQuiet(wsid, {
          file: {
            name,
            p
          }
        })
      })
    }

    //  Start download
    dest = app.util.base.pathJoin(uploadDir, fixname)
    const fileStream = fs.createWriteStream(dest)
    res
      .pipe(str)
      .pipe(fileStream)

    //  Check file type
    res.once('data', (chunk) => {
      const type = fileType(chunk)
      const checkType = whiteList.indexOf(type.ext) !== -1
      if (!checkType) {
        res.destroy()
        if (str) str.destroy()
        fileStream.destroy()
        ws.sendMsgByIdQuiet(wsid, {
          file: {
            name,
            err: 'File type invalid'
          }
        })
      }
    })

    fileStream.on('finish', async () => {
      const fromOptions = {
        fromMail,
        fromPwd,
        fromSmtp
      }
      const toOptions = {
        filepath: dest,
        filename: name,
        to: toMail
      }

      const mailer = new Mailer(fromOptions)

      try {
        await mailer.verify()
        await mailer.send(toOptions)
        mailer.close()
      } catch (err) {
        ws.sendMsgByIdQuiet(wsid, {
          file: {
            name,
            err: err.toString()
          }
        })
      }

      ws.sendMsgByIdQuiet(wsid, {
        file: {
          name,
          mail: true
        }
      })
    })
  }).on('error', err => {
    fs.unlink(dest)
    ws.sendMsgByIdQuiet(wsid, {
      file: {
        name,
        err: err.toString()
      }
    })
  })
}

module.exports = main
