'use strict'

const util = require('util')

const nodemailer = require('nodemailer')

class Mailer {
  constructor ({ fromMail, fromPwd, fromSmtp }) {
    this.transporter = nodemailer.createTransport({
      host: fromSmtp.host,
      port: fromSmtp.port,
      secure: fromSmtp.secure,
      auth: {
        user: fromMail,
        pass: fromPwd
      },
      logger: false,
      debug: false
    }, {
      from: `PushToKindle <${fromMail}>`
    })
  }

  async verify () {
    const { transporter } = this
    await util.promisify(transporter.verify).bind(transporter)()
  }

  async send ({ filename, filepath, to }) {
    const { transporter } = this
    const sendOptions = {
      to,
      attachments: [{
        filename,
        path: filepath
      }]
    }

    await util.promisify(transporter.sendMail).bind(transporter)(sendOptions)
  }

  close () {
    this.transporter.close()
  }
}

module.exports = Mailer
