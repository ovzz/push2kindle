'use strict'

module.exports = async (ctx, next) => {
  const { body } = ctx.request
  const { url: fileURL, to: toMail, from: fromMail, pwd: fromPwd, smtp: fromSmtp } = body

  //  Check must need argvs
  if (fileURL === undefined || toMail === undefined) {
    ctx.status = 401
    ctx.body = {
      msg: 'Needs url or to email!'
    }
    return
  }

  if (fromMail === undefined || fromPwd === undefined || fromSmtp === undefined) {
    const { app } = ctx
    const { email = {} } = app.config
    if (email.passwd === undefined || email.user === undefined) {
      ctx.status = 401
      ctx.body = {
        msg: 'Needs from email account!'
      }
      return
    }
    const { user, passwd, smtp } = email

    Object.assign(body, {
      from: user,
      pwd: passwd,
      smtp
    })
  }

  await next()
}
