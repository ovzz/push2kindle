'use strict'

const downloadFile = require('../service/downloadFile')

module.exports = async (ctx, next) => {
  const { app } = ctx
  const { validator } = app.util.base
  const { body } = ctx.request
  const {
    url: fileURL,
    headers,
    name: fileName,
    to: toMail,
    from: fromMail,
    pwd: fromPwd,
    smtp: fromSmtp,
    wsid
  } = body

  //  Check url
  const isURL = validator.isURL(fileURL)
  const istoMail = validator.isEmail(toMail)
  const isFromMail = validator.isEmail(fromMail)
  if (!isURL || !istoMail || !isFromMail || fromPwd === undefined) {
    ctx.status = 401
    ctx.body = {
      msg: 'Invalid body'
    }
  }

  //  Check to mail address is official kindle address
  const emailWhiteList = app.config.email.whiteList
  const emailSuffix = toMail.replace(/.*@/, '')
  const emailSuffixValid = emailWhiteList.indexOf(emailSuffix) !== -1
  if (!emailSuffixValid) {
    ctx.status = 401
    ctx.body = {
      msg: 'To email should be official kindle address'
    }
    return
  }

  //  Download file server, move controller to WebSocket
  downloadFile({ app, fileURL, headers, fileName, toMail, fromMail, fromPwd, fromSmtp, wsid })

  //  Return 202 accept
  ctx.status = 202
}
