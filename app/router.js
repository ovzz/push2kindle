'use strict'

const Router = require('koa-router')

const pushController = require('./controller/push')
const pretreatPushBody = require('./middleware/pretreatPushBody')

const app = new Router()

app.post('/push', pretreatPushBody, pushController)
app.get('/', async (ctx) => {
  console.log(ctx)
  ctx.body = 2333
})

module.exports = app
