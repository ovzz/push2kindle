'use strict'

const util = require('util')
const fs = require('fs')
const path = require('path')

const uuid = require('uuid/v4')
const validator = require('validator')
const _ = require('lodash')

class Base {
  static pathJoin (...args) {
    return path.join(...args)
  }
  static promisify (fn) {
    return util.promisify(fn)
  }
  static async stat (filepath) {
    const result = await this.promisify(fs.stat)(filepath)
    return result
  }
  static async readFile (...args) {
    const result = await this.promisify(fs.readFile)(...args)
    return result
  }
  static get validator () {
    return validator
  }
  static get _ () {
    return _
  }
  static uuid () {
    return uuid()
  }
}

class Config {
  static get configPath () {
    return Base.pathJoin(__dirname, '..', '..', 'config', 'config.json')
  }
  static async fileExist () {
    let result

    const { configPath } = this
    try {
      await Base.stat(configPath)
      result = true
    } catch (e) {
      if (e.code === 'ENOENT') {
        result = false
      }
      throw e
    }

    return result
  }
  static async get () {
    const { configPath } = this
    const buffer = await Base.readFile(configPath)
    const config = buffer.toString()
    return JSON.parse(config)
  }
}

module.exports = {
  base: Base,
  config: Config
}
