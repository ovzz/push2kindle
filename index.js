'use strict'

const http = require('http')

const Koa = require('koa')
const bodyParser = require('koa-bodyparser')

const WSServer = require('./ws-server')
const router = require('./app/router')
const util = require('./app/util')
const defaultConfig = require('./config/default')

const app = new Koa()

//  Load util to app
app.util = util

//  Load default config to app
app.config = defaultConfig
let privateConfig = {}
try {
  privateConfig = require('./config/private')
} catch (e) {
  console.log('No private config')
}
app.util.base._.merge(app.config, privateConfig)

//  Test catch all error
app.use(async (ctx, next) => {
  try {
    await next()
  } catch (e) {
    console.log(e)
    ctx.status = 500
  }
})

//  Use bodyParser
app.use(bodyParser())

//  Load app to ctx for everyroute
app.use(async (ctx, next) => {
  ctx.app = app
  await next()
})

//  Use router
app
  .use(router.routes())
  .use(router.allowedMethods())

const server = http.createServer(app.callback())

//  Init websocket server
const ws = new WSServer(server)
app.ws = ws

server.listen(app.config.server.port, app.config.server.addr, () => console.log(`Server running at ${app.config.server.addr}:${app.config.server.port}`))
